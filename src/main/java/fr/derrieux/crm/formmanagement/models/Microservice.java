package fr.derrieux.crm.formmanagement.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "microservice_version")
public class Microservice {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) 
    private Long id;

    @Column(length = 10, nullable = false)
    private String version_number;

    @Column(length = 200, nullable = true)
    private String description;

    @Column(nullable = false)
    private Long id_microservice;

    @ManyToOne
    @JoinColumn(name = "id_microservice", insertable = false, updatable = false)
    private MicroserviceInformations microservice_infos;
}

package fr.derrieux.crm.formmanagement.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "instance")
public class Instance {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 50, nullable = false)
    private String name;

    @Column(length = 99, nullable = false)
    private String dns;

    @Column(nullable = false)
    private String siret;

    @Column(nullable = false)
    private Long id_customer;

    @Column(nullable = false)
    private Long id_contract;

    @ManyToOne
    @JoinColumn(name = "id_customer", insertable = false, updatable = false)
    private Customer customer;

    @OneToOne
    @JoinColumn(name = "id_contract", insertable = false, updatable = false)
    private Contract contract;
}

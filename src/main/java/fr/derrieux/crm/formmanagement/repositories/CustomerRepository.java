package fr.derrieux.crm.formmanagement.repositories;

import org.springframework.data.repository.CrudRepository;

import fr.derrieux.crm.formmanagement.models.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Long>{

}

package fr.derrieux.crm.formmanagement.repositories;

import org.springframework.data.repository.CrudRepository;

import fr.derrieux.crm.formmanagement.models.Instance;

public interface InstanceRepository extends CrudRepository<Instance, Long>{

}
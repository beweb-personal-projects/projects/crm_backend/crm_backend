package fr.derrieux.crm.formmanagement.repositories;

import org.springframework.data.repository.CrudRepository;

import fr.derrieux.crm.formmanagement.models.Microservice;

public interface MicroserviceRepository extends CrudRepository<Microservice, Long>{

}
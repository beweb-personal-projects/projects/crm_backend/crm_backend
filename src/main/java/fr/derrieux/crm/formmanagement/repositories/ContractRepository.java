package fr.derrieux.crm.formmanagement.repositories;

import org.springframework.data.repository.CrudRepository;

import fr.derrieux.crm.formmanagement.models.Contract;

public interface ContractRepository extends CrudRepository<Contract, Long>{

}
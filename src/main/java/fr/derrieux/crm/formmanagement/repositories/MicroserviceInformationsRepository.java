package fr.derrieux.crm.formmanagement.repositories;

import org.springframework.data.repository.CrudRepository;

import fr.derrieux.crm.formmanagement.models.MicroserviceInformations;

public interface MicroserviceInformationsRepository extends CrudRepository<MicroserviceInformations, Long>{

}